# hexo-trytry

試試 hexo

> https://hexo.io/zh-tw/

## init
> https://hexo.io/zh-tw/docs/setup

真的會要一個空的目錄，建議先 `hexo init` 後再 `git init` 設定。

執行指令的 terminal **需要有 `git` 在環境變數**中。

原來官方文件上有說，只是是寫在[〈指令〉](https://hexo.io/zh-tw/docs/commands#init)章節......：
> 1. Git clone [hexo-starter](https://github.com/hexojs/hexo-starter) including [hexo-theme-landscape](https://github.com/hexojs/hexo-theme-landscape) into the current directory or a target folder if specified.
> 2. Install dependencies using a package manager: Yarn 1, pnpm or npm, whichever is installed; if there are more than one installed, the priority is as listed. npm is bundled with Node.js by default.


## theme
### 候選
* https://molunerfinn.com/hexo-theme-melody-doc/
